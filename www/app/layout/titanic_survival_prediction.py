# import TPOTClassifier
from tpot import TPOTClassifier
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
import pandas as pd
import string

def substrings_in_string(big_string, substrings):
    for idx, substring in enumerate(substrings):
        if substring in big_string:
            return idx
    print(big_string)
    return np.nan

title_list=['Mrs', 'Mr', 'Master', 'Miss', 'Major', 'Rev',
                    'Dr', 'Ms', 'Mlle','Col', 'Capt', 'Mme', 'Countess',
                    'Don', 'Jonkheer']

# Get train data
train_df = pd.read_csv('/home/chirag/Downloads/train.csv')
# Get test data
test_df = pd.read_csv('/home/chirag/Downloads/test.csv')

train_df['Title']= train_df['Name'].map(lambda x: substrings_in_string(x, title_list))
test_df['Title']= test_df['Name'].map(lambda x: substrings_in_string(x, title_list))
# Extract train features
train_features =  train_df[['Pclass', 'Age', 'Sex', 'Fare', 'Title', 'Survived']].copy()
train_features = train_features.dropna()
train_features.loc[train_features['Sex']=='male', 'Sex'] = 0
train_features.loc[train_features['Sex']=='female', 'Sex'] = 1
# class labels
labels = train_features['Survived'].values.astype(int)
train_features.drop(['Survived'], axis=1)
features = train_features.values.astype(float)

# Extract test features
test_features =  test_df[['Pclass', 'Age', 'Sex', 'Fare', 'Title']].copy()
test_features.loc[test_features['Sex'] == 'male', 'Sex'] = 0
test_features.loc[test_features['Sex'] == 'female', 'Sex'] = 1
test_features = test_features.fillna(-1)
t_features = test_features.values.astype(float)

# result = test_df[['PassengerId']].copy()
# clf = GaussianNB()
# result['Survived'] = clf.predict(t_features)

tpot = TPOTClassifier(generations=5, population_size=20, verbosity=2)
tpot.fit(features, labels)

RandomForestClassifier(input_matrix, RandomForestClassifier__bootstrap=DEFAULT, RandomForestClassifier__criterion=DEFAULT, RandomForestClassifier__max_features=1.0, RandomForestClassifier__min_samples_leaf=8, RandomForestClassifier__min_samples_split=17, RandomForestClassifier__n_estimators=DEFAULT)