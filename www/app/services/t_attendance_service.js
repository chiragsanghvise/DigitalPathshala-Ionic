(function (app) {

    function Service($http, $q,APIUrl) {
        this._$http = $http;
        this._$q = $q;
        this._url=APIUrl;
    }
    // method to get menu_list to display on dashboard based on given data.
    Service.prototype.get_standard_list = function () {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token')
        var url = this._url+"attendancestandard";
           this._$http({
                method: 'GET',
                url: url,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.prototype.get_attendance_detail = function (std_details) {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token');
        var url = this._url+"AttendanceDetail/" + std_details['std'] +"/"+ std_details['cls'] +"/" + std_details['days_count'];
        this._$http({
            method: 'GET',
            url: url,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    };

    Service.prototype.send_attendance_details = function (AttendanceDetail) {
        console.log("Attendance",JSON.stringify(AttendanceDetail));
        var deferred = this._$q.defer();
        var url = this._url+"StudentAttendance";
        this._$http({
            method: 'POST',
            url: url,
            data:AttendanceDetail
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    };
    Service.$inject = ['$http', '$q'    ,'APIUrl'];
    app.service('TAttendanceService', Service);
})(appMain) 