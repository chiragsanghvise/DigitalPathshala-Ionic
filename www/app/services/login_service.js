(function (app) {

    function Service($http, $q,APIUrl) {
        this._$http = $http;
        this._$q = $q;
        this._url=APIUrl;

        function register(device_token){
            console.log("Device_Token",device_token);   
            return device_token;    
        };

        return {
            register: register
        };

    }

    Service.prototype.login = function (logindata) {
        var deferred = this._$q.defer();
        console.log('logindata',JSON.stringify(logindata));
        var url = this._url+"Authenticate/AuthorizationToken";
           this._$http({
                 // method:'GET',
                 // url:'txt/teacher_dash.txt'      
                method: 'POST',
                 url: url,
                 data: logindata,
            }).success(function (data, status, headers, cfg) {
                if (status == 404)
                    data = {'error':'Not Found'}
                deferred.resolve(data);
            }).error(function (err, status) {
                console.log('err');
                deferred.reject(status);
            });
            return deferred.promise;
    };
    Service.$inject = ['$http', '$q','APIUrl'];
    app.service("loginService", Service);
})(appMain)