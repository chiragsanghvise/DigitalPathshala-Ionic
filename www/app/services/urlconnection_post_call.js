(function (app) {

    function Service($http, $q,APIUrl) {
        this._url=APIUrl;
        this._$http = $http;
        this._$q = $q;
    }

    Service.prototype.post = function (content,post_url) {
        var deferred = this._$q.defer();
        console.log("url",post_url);
           this._$http({
                 method: 'POST',
                 url: post_url,
                 data: content,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
            return deferred.promise;
    };
    Service.$inject = ['$http', '$q','APIUrl'];
    app.service("PostCallService", Service);
})(appMain)