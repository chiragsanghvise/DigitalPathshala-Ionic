(function (app) {

    function Service($http, $q) {
        this._$http = $http;
        this._$q = $q;
    }

    Service.prototype.applyleave = function (leavedata) {
        var deferred = this._$q.defer();
        console.log('leavedata',JSON.stringify(leavedata));
        var url = "http://school.jmsofttech.com/api/TeacherLeave";
           this._$http({
                 method: 'POST',
                 url: url,
                 data: leavedata,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
            return deferred.promise;
    };
    Service.$inject = ['$http', '$q'];
    app.service("LeaveApplyService", Service);
})(appMain)