(function (app) {
    
    function Service($http, $q,APIUrl) {
        this._$http = $http;
        this._$q = $q;
        this._url=APIUrl;
    }

    Service.prototype.get_leave_details = function (teacher_info) {
        var deferred = this._$q.defer();
        console.log(teacher_info);
        var url = this._url+"ViewTeacherLeave/"+teacher_info['Id'];
           this._$http({
                method: 'GET',
                url: url,
                // data: logindata,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.prototype.post = function (content) {
        var deferred = this._$q.defer();
        post_url=this._url+"ApproveTeacherLeave";
       // console.log("url",post_url);
           this._$http({
                 method: 'POST',
                 url: post_url,
                 data: content,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.$inject = ['$http', '$q','APIUrl'];
    app.service("PTLeaveService", Service);
})(appMain)