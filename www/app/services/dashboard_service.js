(function (app) {

    function Service($http, $q,APIUrl) {
        this._url=APIUrl;
        this._$http = $http;
        this._$q = $q;
    }
    // method to get menu_list to display on dashboard based on given data.
    Service.prototype.get_names = function () {
        var userType = window.localStorage.getItem('UserType');
        console.log(userType);
        var deferred = this._$q.defer();
        var dash_url = this._url+"Menu/"+userType;
        console.log(dash_url);
        this._$http({
                  method:'GET',
                  url: dash_url     
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
             return deferred.promise;
    };

    Service.prototype.get_selected_menu_id = function(data){
        return data
    }


    Service.$inject = ['$http', '$q','APIUrl'];

    app.service("dashService", Service);
})(appMain)