(function (app) {

    function Service($http, $q,APIUrl) {
        this._$http = $http;
        this._$q = $q;
        this._url=APIUrl;
    }
    // method to get menu_list to display on dashboard based on given data.
    Service.prototype.get_exam_tt_data = function () {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token')
        var url = this._url+"StandardExamMaster";
           this._$http({
                method: 'GET',
                url: url,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.prototype.get_schedule = function(selected_chaptr){
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token')
       // console.log(selected_chaptr);
        var url = this._url+"ExamTimeTable/9480/9800/"+selected_chaptr['Id'];
        this._$http({
            method: 'GET',
            url: url,
            // data: logindata,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;    
    };

    Service.$inject = ['$http', '$q','APIUrl'];

    app.service("examTimeTableService", Service);
})(appMain) 