(function (app) {

    function Service($http, $q,APIUrl) {
        this._$http = $http;
        this._$q = $q;
        this._url = APIUrl;
    }
    // method to get menuList to display on dashboard based on given data.
    Service.prototype.GetStandardList = function () {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token')
        var url = this._url+"StandardClass";
        console.log("data",data);
           this._$http({
                method: 'GET',
                // url:'holiday.txt'
                url: url,
           }).success(function (data, status, headers, cfg) {
                console.log("success",status);
                deferred.resolve(data);
            }).error(function (err, status) {
                console.log("fail",err);
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.prototype.get_exam_list = function (std) {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token');
        var url = this._url+"StandardExamMaster/" + std;
        this._$http({
            method: 'GET',
            url: url,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    };

    Service.prototype.get_exam_schedule = function (params) {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token');
        var url = this._url+"ExamSchedule/" + params['std'] + "/"+params['exam_id'];
        this._$http({
            method: 'GET',
            url: url,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    };

    Service.prototype.get_student_list = function (params) {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token');
        var url = this._url+"ViewStudentMarks/" +params['std'] + "/"+ params['cls'] + "/" + params['exam_id'] + "/" + params['scheduleid'];
        this._$http({
            method: 'GET',
            url: url,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    };

    Service.prototype.update_students_marks = function (studentsDetail) {
        // console.log(JSON.stringify(studentsDetail));
        var deferred = this._$q.defer();

        var data = window.localStorage.getItem('Token');
        var url = this._url+"StudentMarks";
        this._$http({
            method: 'POST',
            url: url,
            data:studentsDetail,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    };


    Service.$inject = ['$http', '$q','APIUrl'];

    app.service("TUploadPaperService", Service);
})(appMain) 