angular.module('starter')

.controller('HolidayCtrl', function($scope, $state, $ionicPopup, $window, $location, HolidayService) {
	
	// call service method to get menu_list and pass it to html page
	HolidayService.get_holiday_list().then(function(holidays){
		if(holidays.length==0){
			$ionicPopup.alert({
				template:"No data is available for Holidays",					
			});
		}else{
			$scope.holiday_list = holidays;
		}
	},function(msg){
		console.log("ctrl","msg");
	});

	$scope.dateFormatter = function(date){
		console.log(date.split(' ')[0]);
		return date;
	}

})
 
