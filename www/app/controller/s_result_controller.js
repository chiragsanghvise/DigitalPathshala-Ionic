angular.module('starter')

.controller('SResultCtrl', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl) {
//	var api_url="http://school.jmsofttech.com/api/";
	$ionicLoading.show();
	$http.get(APIUrl+'StandardExamMaster')
	.success(function(data){
		$ionicLoading.hide();
		if (data.length==0) {
			$ionicPopup.alert({
				template:"Exam Data is not available",					
			});
		}else{
			$scope.examtype_list=data;	
		}		
	}).error(function(err){
		$ionicLoading.hide();
	})

	$scope.examId = function(item) {
		$ionicLoading.show();
		$http.get(APIUrl+'ViewResult/'+item.Id)
			.success(function(data){
				$ionicLoading.hide();
				if(JSON.stringify(data)=="null"){
					$ionicPopup.alert({
						template:"Result Data is not available for this Exam",					
					});
					$scope.result_available=true;
				}else{
					$scope.result_data=data;
						$scope.result_available=false;
				}
		//		console.log("data",JSON.stringify(data));
			}).error(function(err){
				$ionicLoading.hide();
			})
	//	 AuthService.login(data.username, data.password);
		//$state.go('result');
	}	 
})