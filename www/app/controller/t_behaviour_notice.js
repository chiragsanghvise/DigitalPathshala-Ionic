angular.module('starter')

.controller('TBehaviourNotice', function($scope, $state,$http ,$ionicPopup,$ionicLoading,PostCallService,APIUrl) {
	// var api_url="http://school.jmsofttech.com/api/";
		
		$ionicLoading.show();
		var _std;
		var	_cls;
		$http.get(APIUrl+'standardClass')
		.success(function(data){
			$ionicLoading.hide();
			if(JSON.stringify(data)!="[]"){
				$scope.hasStandard=true;	
				$scope.standard_list=data;
			}else{
				$ionicPopup.alert({
					template:"Standard data is not available",					
				});
				$scope.hasStandard=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
		})

		$scope.getStdId=function(std){
			id=std.Id;	
			_std=id.substring(0,id.indexOf("_"));
			_cls=id.substring(id.indexOf("_")+1,id.length);
			$ionicLoading.show();	
			$http.get(APIUrl+'Students/'+_std+'/'+_cls)
			.success(function(data){
				$ionicLoading.hide();
				if(JSON.stringify(data)!="[]"){
					$scope.hasStudents=true;	
					$scope.student_list=data;
				}else{
					$ionicPopup.alert({
						template:"No Student record for this Standard",					
					});
					$scope.hasStudnets=false;
				}
			}).error(function(err){
				$ionicLoading.hide();
			})				
		}

		$scope.getStudentId=function(student){
		//	$scope.student_obj=student;
			$scope.stud_data = { student_name : student.Name};

			 var myPopup = $ionicPopup.show({
      					templateUrl: 'app/layout/t_dialog_behaviour_notice.html',
      					title: "Notice",
      					scope: $scope,	
      					 buttons: [{
					         text: 'Cancel',
							}, {
							 text: '<b>Save</b>',
							 type: 'button-positive',
							 onTap:function(){
							 	if ($scope.stud_data.comment=="") {
							 		$ionicPopup.alert({
										template:"Please Enter Comment",					
									});		
							 	}else{
							 		var _jsonNotice='{"StandardId":'+_std+', "ClassTypeId":'+_cls+', "StudentId":'+student.Id+',"Comment":"'+$scope.stud_data.comment+'"}'
									saveComplain(_jsonNotice);				
							 	};
							 		
							 }	
							}]
				});
		}
		var saveComplain=function(complain){
			var _postUrl=APIUrl+"StudentBehaviourNotice";
			$ionicLoading.show();	
			PostCallService.post(complain,_postUrl).then(function(status){
				$ionicLoading.hide();	
				$ionicPopup.alert({
					title:"success",
					template:"Your Comment sent successfully",					
				});
			})	
		}

})