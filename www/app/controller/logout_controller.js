angular.module('starter')

.controller('logoutCtrl', function($scope, $state, $location) {
	window.localStorage.removeItem('Token');
	window.localStorage.removeItem('UserType');
	$location.path('/login')
})
 
