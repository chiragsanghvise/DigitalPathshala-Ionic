angular.module('starter')

.controller('SComplainDetailCtrl', function($scope, $state,$http ,$ionicLoading,$ionicPopup,$stateParams,complainService,PostComplainService,APIUrl) {
	
	var getComplainData= function(){
		//$ionicLoading.show();	
		$http.get(APIUrl+'ViewComplainDetail/'+complainService.getId())
		.success(function(data){
		//	$ionicLoading.hide();
			if(data=="[]"){
				detail_available=true;				
			}else{
				$scope.complain_detail_data=data;
				detail_available=false;
			}
		}).error(function(err){
		//	$ionicLoading.hide();
		});
	
	}
	getComplainData();
	$scope.sendComplains=function(postComplain){

		var complain='{"StudentBehaviourNoticeId":'+complainService.getId()+', "Comment":"'+postComplain.message+'"}';
		PostComplainService.postComplain(complain).then(function(complainData){
			$ionicPopup.alert({
				title:'Success',
				template:complainData,					
			});
		});	
		getComplainData();
		postComplain.message="";
	}
	
})
