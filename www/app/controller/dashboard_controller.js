angular.module('starter')

.controller('DashCtrl', function($scope, $state, $ionicPopup, $http, $window, $location, $rootScope, $ionicLoading, dashService,APIUrl) {
	// call service method to get menu_list and pass it to html page
	
	$ionicLoading.show();
	dashService.get_names().then(function(dashboard_data){
		$ionicLoading.hide();		
		// console.log(dashboard_data);			
		$rootScope.menu_list = dashboard_data; 	
		$scope.widthinPX = Math.floor($window.innerWidth / 4);
	},function(msg){
		console.log("ctrl","msg");
	});


	// based on selected menu, redirect to module	
	$scope.get_menu_list_item = function(data) {
		menu_type = dashService.get_selected_menu_id(data)		
		console.log(menu_type);
		switch(menu_type){
			// if user selects attendance, redirect to attendance module
			case 1:
				$location.path('/t_attendance');
				break;
			case 2:
				$location.path('/teacherhomework');
				break;	
			case 3:
				$location.path('/stduentbehaviournotice');
				break;
			case 4:
				$location.path('/teacherlogbook');
				break;
			case 5:
				$location.path('/teacherapplyleave');
				break;
			case 6:
				$location.path('/teacherschedule');
				break;
			case 7:
				$location.path('/t_exammark');
				break;
			case 8:
				$ionicPopup.alert({
					template: 'Under development'
				});
				// $location.path('/t_attendance');
				break;
			case 9:
				$location.path('/notification');
				break;										
			case 10:
				$location.path('/attendance');
				break;
			case 12:
				$location.path('/examtimetable');
				break;
			case 13:
				$location.path('/stud_result');
				break;
			case 14:
				$location.path('/stud_view_complain');
				break;
			case 15:
				$location.path('/homework');
				break;
			case 16:
				$location.path('/notification');
				break;
			case 17:
				$location.path('/complainbox');
				break;
			case 18:
				$location.path('/timetable');
				break;
			case 19:
				$location.path('/headcount');
				break;		
			case 21:
				$location.path('/viewadmission');
				break;
			case 22:
				$location.path('/teacherleave');
				break;
			case 26:
				$location.path('/teacherlist');
				break;					
			case 24:
				$ionicPopup.alert({
					template: 'Under development'
				});
				// $location.path('/uploadedpaper');
				break;
			case 25:
				$location.path('/holidaylist');
				break;
			case 23:
				var confirmPopup = $ionicPopup.confirm({
					title: 'Logout',
					template: 'Are you sure, you want to logout?',
					cancelText: 'No', // String (default: 'Cancel'). The text of the Cancel button.
				  	cancelType: 'button-positive', // String (default: 'button-default'). The type of the Cancel button.
				  	okText: 'Yes', // String (default: 'OK'). The text of the OK button.
				  	okType: 'button-positive', // String (default: 'button-positive'). The type of the OK button.
				});
				confirmPopup.then(function(res){
					if(res){
						$location.path('/logout');
					}
				})
				break;
			case 27:
				$location.path('/notification');
				break;
			case 28:
				$ionicPopup.alert({
					template: 'Under development'
				});
				break;
			case 29:
				$location.path('/stodaytimetable');
				break;	
			default:
				$location.path('/dashboard');
		}	
	}	 
})
 
