angular.module('starter')

.controller('PTimetable', function($scope, $state,$http ,$ionicPopup,$ionicLoading) {
	var api_url="http://school.jmsofttech.com/api/";
	

	$scope.getUserType = function(choice){
		$ionicLoading.show();	
		if(choice=="Teacher"){
				$scope.isTeacher=true;
				$scope.isStudent=false;
				$scope.isTTimetable=false;
				$scope.isSTimetable=false;	
				$http.get(api_url+'Teacher')
				.success(function(data){
					$ionicLoading.hide();
					$scope.teacher_list=data;
				}).error(function(err){
					$ionicLoading.hide();
				})
			}else if(choice=="Student"){
				$scope.isTeacher=false;
				$scope.isStudent=true;
				$scope.isTTimetable=false;
				$scope.isSTimetable=false;
				$http.get(api_url+'StandardClass')
				.success(function(data){
					$ionicLoading.hide();
					$scope.student_list=data;
				}).error(function(err){
					$ionicLoading.hide();
				})
			}	
	}

	$scope.teacherId=function(teacher){
		$http.get(api_url+'TimeTable/'+teacher.Id)
		.success(function(data){
			if(JSON.stringify(data)!="[]"){
				$scope.isTTimetable=true;
				$scope.schedule=data;
			}else{
				$scope.isTTimetable=false;
				$ionicPopup.alert({
					template:"No timetable data is available for this professor",					
				});	
			}
		}).error(function(err){
			$ionicLoading.hide();
		})
	}

	$scope.studentId=function(student){
		id=student.Id;	
		std=id.substring(0,id.indexOf("_"));
		cls=id.substring(id.indexOf("_")+1,id.length);	
	
		$http.get(api_url+'ShowTimeTable/'+std+'/'+cls)
		.success(function(data){
			if(JSON.stringify(data)!="[]"){
				$scope.isSTimetable=true;
				$scope.stud_schedule=data;
			}else{
				$scope.isSTimetable=false;
				$ionicPopup.alert({
					template:"No timetable data is available for this class",					
				});	
			}
		}).error(function(err){
			$ionicLoading.hide();
		})
	}
})