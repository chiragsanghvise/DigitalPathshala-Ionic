angular.module('starter')

.controller('TempCtrl', function($scope, $state, $ionicPopup, $cordovaImagePicker ,$location, $ionicLoading, $ionicPlatform, $ionicHistory, loginService) {
$scope.collection = {
  selectedImage : ''
};
$scope.ImageURI = 'file:///data/data/com.ionicframework.digitalpathshala939451/cache/tmp_IMG-20170515-WA00032105967675.jpg';

    function UploadPicture(imageURI) {

        $scope.ImageURI =  imageURI;
        
        alert($scope.ImageURI );
    }

var options = {
  maximumImagesCount: 10,
  width: 800,
  height: 800,
  quality: 80
   };

$scope.openGallery = function() {
/*	$cordovaImagePicker.getPictures(options)
   .then(function (results) {
     	for (var i = 0; i < results.length; i++) {
       	console.log('Image URI: ' + results[i]);
     	}
   	}, function(error) {
     	// error getting photos
   	});*/
    /*navigator.camera.getPicture(onSuccess, onFail,
    {
       destinationType: Camera.DestinationType.FILE_URI,
       sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
       popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY)
    });

    window.onorientationchange = function() {
       var cameraPopoverHandle = new CameraPopoverHandle();
       var cameraPopoverOptions = new CameraPopoverOptions(0, 0, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY);
       cameraPopoverHandle.setPosition(cameraPopoverOptions);
    }*/
    var options = {
        maximumImagesCount: 5, // Max number of selected images, I'm using only one for this example
        width: 800,
        height: 800,
        quality: 80            // Higher is better
    };
 
    $cordovaImagePicker.getPictures(options).then(function (results) {
                // Loop through acquired images

        for (var i = 0; i < results.length; i++) {
          console.log('Image URI: ' + results[i]);   // Print image URI
          window.plugins.Base64.encodeFile(results[0], function(base64){
            $scope.ImageURI =  base64;
            $scope.collection.selectedImage = base64;
            // console.log('file base64 encoding: ' + $scope.ImageURI);
          });
            
        }
        
    }, function(error) {
        console.log('Error: ' + JSON.stringify(error));    // In case of error
    });

};
})