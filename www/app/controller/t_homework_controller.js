angular.module('starter')

.controller('THomeWork', function($scope, $http, $ionicPopup, $ionicLoading, ionicDatePicker, PostHomeWorkService, APIUrl) {
	//var api_url="http://school.jmsofttech.com/api/";
	var days;
	var _lectureId;
	var date = null;
	$scope.currentDate = getCurrentDate(new Date());
	$scope.getLectureForDate=function(){
		// If date is not given, by default today's date is taken.
		if(!date){
			date = new Date();
		}
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		days = ((((diff / 1000) / 60) / 60) / 24);
		$ionicLoading.show();
		$http.get(APIUrl+'LectureByDate/'+days)
		.success(function(data){
			$ionicLoading.hide();
			
			if(JSON.stringify(data)!="[]"){
				$scope.hasLecture=true;
				$scope.lecture_list=data;
			}else{
				$ionicPopup.alert({
					template:"Lecture Data is not available",					
				});
				$scope.hasLecture=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
		})
	}

	$scope.getLectureId=function(lecture){
		// if no lecture selected hide homework div and return
		if(!$scope.hasLecture || !lecture){
			$scope.hasHomework=false;
			return;
		}
		_lectureId=lecture.Id;
		$ionicLoading.show();
		$http.get(APIUrl+'ViewHomeWork/'+days+'/'+lecture.Id)
		.success(function(data){
			$ionicLoading.hide();
			if(JSON.stringify(data)!="null"){
	/*			alert("true");*/
				$scope.hasHomework=true;
				$scope.lesson=data;
			}else{
			/*	alert("false");
				$ionicPopup.alert({
					template:"No Homework Detail Available",					
				});*/
				$scope.lesson = {}				
				$scope.hasHomework=true;
			}
		}).error(function(err){
			$ionicLoading.hide();
		})
	}

	$scope.saveHomework=function(lesson){
		if (lesson.HomeWork=="") {
			$ionicPopup.alert({
				template:"Please Enter HomeWork!",					
			});
		}else if (lesson.ClassWork=="") {
			$ionicPopup.alert({
				template:"Please Enter ClassWork!",					
			});	
		}else{
			var _JsonData='{"Date":'+days+' , "LectureId":'+_lectureId+',"HomeWork":"'+lesson.HomeWork+'","ClassWork":"'+lesson.ClassWork+'"}'
			$ionicLoading.show();
			PostHomeWorkService.postHomework(_JsonData).then(function(success){
				$ionicLoading.hide();
				$ionicPopup.alert({
					template:success,					
				});	
			})
		}
	};

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
		var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                        "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        return day + " " + monthNames[month] + ", " + year;
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	date = selectedDate;
        	$scope.getLectureForDate();
        	$scope.currentDate = getCurrentDate(selectedDate);
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
		
})