angular.module('starter')

.controller('SSamplePaper', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl) {
	$scope.images = [];

	$scope.loadSubjects=function(){
		$ionicLoading.show();
		$http.get(APIUrl+'subject/0')
		.success(function(data){
			$ionicLoading.hide();
			if (JSON.stringify(data)=="[]") {
				$ionicPopup.alert({
					template:"Exam Data is not available",					
				});
			}else{
				$scope.subject_list=data;	
			}		
		}).error(function(err){
			$ionicLoading.hide();
y		})
	}

	$scope.getPaper=function(subjectList){
		$ionicLoading.show();
		$http.get(APIUrl+'ViewExamplePaper/'+subjectList.Id)
		.success(function(data){
			$ionicLoading.hide();
			$ionicLoading.hide();
			if (JSON.stringify(data)=="[]") {
				$ionicPopup.alert({
					template:"Paper is not available",					
				});
			}else{
				$scope.paper_list=data;	
			}		
		}).error(function(err){
			$ionicLoading.hide();
		})		
	}




})
