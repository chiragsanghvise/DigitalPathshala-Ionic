angular.module('starter')

.controller('STodayTimetable', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl) {
	$scope.day_list=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
	
	$scope.weekdays=$scope.day_list[ new Date().getDay()];

	var getId= function(weekdays){
		switch(weekdays){
			case 'Sun':
				return 0;
			case 'Mon':
				return 1;
			case 'Tue':
				return 2;
			case 'Wed':
				return 3;
			case 'Thu':
				return 4;
			case 'Fri':
				return 5;
			case 'Sat':
				return 6;
			default:
				return -1;							
		}
	}

	var fetchSchedule=function(url){
		$ionicLoading.show();	
		$http.get(url)
			.success(function(data){
				$ionicLoading.hide();
				if(JSON.stringify(data)=="[]"){
					$ionicPopup.alert({
						template:"No schedule for this day",					
					});
					$scope.isLectureAvailable=false;
				}else{
					$scope.lecture_schedule=data;
					$scope.isLectureAvailable=true;
				}
		//		console.log("data",JSON.stringify(data));
			}).error(function(err){
				$ionicLoading.hide();
			})		
	}

	var cur_dataUrl=APIUrl+"StudentTimeTable/"+getId($scope.weekdays);
	fetchSchedule(cur_dataUrl);
	$scope.getDaySchedule=function(weekdays){
		var schedule_url=APIUrl+'StudentTimeTable/'+getId(weekdays);
		fetchSchedule(schedule_url);
		
		

	}
})