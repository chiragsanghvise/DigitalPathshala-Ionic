angular.module('starter')

.controller('SComplainCtrl', function($scope, $state,$http ,$ionicPopup,$ionicLoading,complainService,APIUrl) {
	this._url=APIUrl;
	$ionicLoading.show();
	$http.get(this._url+'ViewComplain')
	.success(function(data){
		$ionicLoading.hide();
		if(data=="[]"){
			complain_available=true;	
			$ionicPopup.alert({
				template:"Complain data is not available",					
			});			
		}else{
			$ionicLoading.hide();
			$scope.complain_data=data;
			complain_available=false;
		}
		
	}).error(function(err){
	});
	$scope.getComplainDetails=function(complain){
		complainService.setId(complain.StudentBehaviourNoticeId);
		$state.go('complaindetails');
		
	}
})
