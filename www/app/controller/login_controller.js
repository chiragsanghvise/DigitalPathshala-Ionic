angular.module('starter')

.controller('AppCtrl', function($scope, $state, $ionicPopup, $location, $ionicLoading, $ionicPlatform, $ionicHistory, loginService) {
	
	function loginService($http, $q, $ionicLoading){
		console.log("Service","Yes");

		function register(device_token){

			console.log("Device_Token",device_token);	
			window.localStorage.setItem('deviceToken', device_token);			
			return device_token;			

		};


		return {
			register: register
		};
	}		

	

	$scope.login = function(data) {
		console.log(data);
		// If password or username field is empty, return
		if(!(data) || !(data.password) || !(data.username)){
			$ionicPopup.alert({
				title: 'Login Failed',
				template: 'Please fill all given fields.'
			});
			return;
		}

		$ionicLoading.show();
		loginService.login(data).then(function(loginData){
			if (loginData){
				$ionicLoading.hide();
				if(loginData['error']){
					$ionicPopup.alert({
						title: 'Login Failed',
						template: 'Please enter correct login detail'
					});
				}else{
					window.localStorage.setItem('Token', loginData['Token']);
					window.localStorage.setItem('UserType', loginData['UserType']);
					// $location.path('/dashboard')
					$state.go('dashboard');	
				}
			}
		},function(msg){
			$ionicLoading.hide();	
			console.log("ctrl","msg");
		});
	};

	// handle back button
	// if current page is login, on back exit app
	// if current page is dashboard, dont go back else go back
	$ionicPlatform.registerBackButtonAction(function (evt) {
	    // prevent default backbutton action
	    if($ionicHistory.currentStateName() == 'login'){
	        evt.preventDefault();
	        ionic.Platform.exitApp(); // stops the app
	    }else if($ionicHistory.currentStateName() == 'dashboard'){
	        evt.preventDefault();
	    }else{
	    	$ionicHistory.goBack();
	    }
	}, 100);	 
})
 
