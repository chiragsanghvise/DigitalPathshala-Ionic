angular.module('starter')

.controller('TLogBook', function($scope, $http, $ionicPopup, $ionicLoading, ionicDatePicker, PostCallService, APIUrl) {
//	var api_url="http://school.jmsofttech.com/api/";
	var _date=9696;
	var _lecture_id;
	var _lessonPlan;
	var _teachingAids;
	var _comment;
	var diff;
	var days;
	var date = null;
	$scope.currentDate = getCurrentDate(new Date());

	$scope.getLectureForDate=function(){
		// If date is not given, by default today's date is taken.
		if(!date){
			date = new Date();
		}
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		days = ((((diff / 1000) / 60) / 60) / 24);
		$ionicLoading.show();
		$http.get(APIUrl+'LectureByDate/'+days)
		.success(function(data){
			$ionicLoading.hide();
			if(JSON.stringify(data)!="[]"){
				$scope.hasLecture=true;
				$scope.lecture_list=data;
			}else{
				$ionicPopup.alert({
					template:"Lecture Data is not available",					
				});
				$scope.hasLecture=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
		})
	};

	$scope.getLectureId=function(lecture){
		if(!lecture || !lecture.Id)
			return;	
		_lecture_id=lecture.Id;
		$ionicLoading.show();
		$http.get(APIUrl+'ViewLogBook/'+days+'/'+lecture.Id)
		.success(function(data){
			$ionicLoading.hide();
			console.log(data);
			$scope.hasRecord=true;
			if(JSON.stringify(data)!="null"){
				$scope.logbook=data;
			}else {
				$scope.logbook={};
			// $scope.hasRecord=false;

			}
		}).error(function(err){
			$ionicLoading.hide();
		})	
	};

	$scope.saveLogbook=function(logbook){
		_lessonPlan=logbook.LessonPlan;
		_teachingAids=logbook.TeachingAids;
		_comment=logbook.Comment;
		if (_lessonPlan=="") {
			$ionicPopup.alert({
				template:"Please Enter Lesson",					
			});
		}else if (_teachingAids=="") {
			$ionicPopup.alert({
				template:"Please Enter TeachingAids",					
			});
		}else if (_comment=="") {
			$ionicPopup.alert({
				template:"Please Enter comment",					
			})
		}else{
			var _posrUrl=APIUrl+"TeacherLogBook";	
			var _jsonLogbook='{"Date":'+days+', "LectureId" : ' +_lecture_id + ', "LessonPlan" : "'+_lessonPlan+'","TeachingAids":"'+_teachingAids+'", "Comment":"'+_comment+'"};';
			PostCallService.post(_jsonLogbook,_posrUrl).then(function(status){
				$ionicPopup.alert({
					template:status,					
				});
			})	
		}
	};

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return year + "-" + month + "-" + day;
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	date = selectedDate;
        	$scope.getLectureForDate();
        	$scope.currentDate = getCurrentDate(selectedDate);
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
})	