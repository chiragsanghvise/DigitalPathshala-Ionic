angular.module('starter')

.controller('TApplyLeave', function($scope, $state, $http, $ionicPopup, $ionicLoading, LeaveApplyService, PostCallService, APIUrl, ionicDatePicker) {
  //var api_url="http://school.jmsofttech.com/api/";
	$scope.currentDate = "Select Date";
	var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                    "July", "Aug", "Sept", "Oct", "Nov", "Dec"];	

  $scope.applyLeave = function(leave) {
  	if($scope.currentDate == 'Select Date'){
  		$ionicPopup.alert({
        template: "Please select appropriate date",
      });
      return;
  	}

  	var diff = new Date($scope.currentDate) - new Date('1990-01-01 00:00:00');

		// Calculate from milliseconds
		var days = ((((diff / 1000) / 60) / 60) / 24);
		leave['Date'] = days;
    var _postUrl = APIUrl + "TeacherLeave";
    if (leave.ReasonOfLeave == "") {
        $ionicPopup.alert({
            template: "Please Enter Reason For Leave",
        });
    } else if (leave.NoOfDays == "") {
        $ionicPopup.alert({
            template: "Please Enter No of days for Leave",
        });
    } else {
        PostCallService.post(leave, _postUrl).then(function(leaveData) {
            if (leaveData.length == 0) {
                $ionicPopup.alert({
                    template: "There is some error,Your leave request have not sent",
                });
            } else {
                clear(leave);
                $ionicPopup.alert({
                    template: "Leave Succefully Applied",
                });
            }
            // alert(JSON.stringify(leaveData));
        }, function(msg) {
        });
    }
  }

  var clear = function(leave) {
      leave.NoOfDays = '';
      leave.ReasonOfLeave = '';
      $scope.currentDate = 'Select Date';
  }

  var ipObj1 = {
    	callback: function (val) {  //Mandatory
      	var selectedDate = new Date(val);
      	var year = selectedDate.getFullYear();
      	var month = selectedDate.getMonth();
      	var day = selectedDate.getDate();
      	$scope.currentDate = day + " " + monthNames[month] + ", " + year;
    	},
	inputDate: new Date(),      //Optional
	mondayFirst: true,          //Optional
	closeOnSelect: false,       //Optional
	templateType: 'popup'       //Optional
  };

  $scope.openDatePicker = function(){
    	ionicDatePicker.openDatePicker(ipObj1);
  };
})