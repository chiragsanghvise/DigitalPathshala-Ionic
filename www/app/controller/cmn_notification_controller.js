angular.module('starter')

.controller('CmnNotification', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl) {
	
	$ionicLoading.show();
	$http.get(APIUrl+'ViewNotification')
		.success(function(notify){
			$ionicLoading.hide();
			if(notify.length){
				$scope.hasNotification=true;
				$scope.notification_list=notify;
			}else{
				$scope.hasNotification=false;
				$ionicPopup.alert({
					template:"Notification is not available",					
				});
			}
		}).error(function(err){
			$ionicLoading.hide();
		})

})