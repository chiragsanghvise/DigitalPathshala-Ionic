angular.module('starter')
.controller('TTodaySchedule', function($scope, $ionicPopup,$http, $ionicLoading,ionicDatePicker,APIUrl) {
	/*var noOfDays=9668;*/
	$scope.days=-1;
	var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                    "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	$scope.currentDate = getCurrentDate(new Date());

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return day + " " + monthNames[month] + ", " + year;
	}
	
	function getNoOfDays(date){
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		return ((((diff / 1000) / 60) / 60) / 24);
	}

	$scope.getScheduleData= function(noOfDays){
		if(noOfDays == '' || !noOfDays){
			noOfDays = getNoOfDays(new Date());
		}
		$scope.days=noOfDays;
		$ionicLoading.show();
		$http.get(APIUrl+'TeacherTimeTable/'+$scope.days)
		.success(function(data){
			$ionicLoading.hide();
			if(JSON.stringify(data)!="[]"){
				$scope.hasSchedule=true;
				$scope.schedule_list=data;
			}else{
				$ionicPopup.alert({
					template:"No schedule for this date"
				});	
				$scope.hasSchedule=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
		})	
	}

	$scope.preDaySchedule= function(){
		$scope.days=$scope.days-1;
		$scope.getScheduleData($scope.days);
		var date = new Date(1990,00,01);
		date.setDate(date.getDate() + $scope.days); 
        $scope.currentDate = getCurrentDate(date);
	};

	$scope.nextDaySchedule= function(){
		$scope.days=$scope.days + 1;
		$scope.getScheduleData($scope.days);
		var date = new Date(1990,00,01);
		date.setDate(date.getDate() + $scope.days); 
        $scope.currentDate = getCurrentDate(date);
	};

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	$scope.getScheduleData(getNoOfDays(selectedDate));
        	$scope.currentDate = getCurrentDate(selectedDate);
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
})