angular.module('starter')

.controller('TAttendanceCtrl', function($scope, $state, $ionicPopup, $location, $ionicLoading, ionicDatePicker, TAttendanceService) {

	$scope.standard = ""
	$scope.std_div = ""
	$scope.is_std_selected = false;
	$scope.is_date_selected = false;
	$scope.currentDate = "Select Date";
	var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                        "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

	// call service method to get menu_list and pass it to html page
	TAttendanceService.get_standard_list().then(function(standards){
		$scope.standard_list = standards;
	},function(msg){
		console.log("ctrl","msg");
	});

	$scope.split_id = function(data){
		ary = data['Id'].split('_');
		$scope.standard = ary[0];
		$scope.std_div = ary[1];
		$scope.is_std_selected = true;
	}

	$scope.get_attendance_detail = function(date){
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = new Date(date) - new Date('1990-01-01 00:00:00');

		// Calculate from milliseconds
		var days = ((((diff / 1000) / 60) / 60) / 24);
		var data = {
			'std': $scope.standard,
			'cls': $scope.std_div,
			'days_count': days
		};
		$ionicLoading.show();
		TAttendanceService.get_attendance_detail(data).then(function(attendance_detail){
			$ionicLoading.hide();
			$scope.attendance_detail = attendance_detail
			$scope.students_list = attendance_detail['Students'];
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
		});		
		$scope.is_date_selected = true;
	}

	$scope.updatePresentStatus = function(student_id,present_status){
		for (var i = 0; i < $scope.students_list.length; i++) {
			if($scope.students_list[i]['StudentId'] == student_id && present_status == true){
				$scope.attendance_detail['Students'][i]['IsPresent'] = 0;
			}else if($scope.students_list[i]['StudentId'] == student_id && present_status == false){
				$scope.attendance_detail['Students'][i]['IsPresent'] = 1;
			} 
		};
	}

	$scope.send_data = function(){
		$ionicLoading.show();
		TAttendanceService.send_attendance_details($scope.attendance_detail).then(function(response){
			$ionicLoading.hide();
			console.log(response)
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
		});	
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	var year = selectedDate.getFullYear();
        	var month = selectedDate.getMonth();
        	var day = selectedDate.getDate();
        	$scope.get_attendance_detail(new Date(val));
        	$scope.currentDate = day + " " + monthNames[month] + ", " + year;
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
})
 
