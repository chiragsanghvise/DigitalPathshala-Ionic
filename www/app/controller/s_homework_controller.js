angular.module('starter')

.controller('SHomework', function($scope, $state,$http,$ionicPopup,$ionicLoading, ionicDatePicker,APIUrl) {
	$ionicLoading.show();
    var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                        "July", "Aug", "Sept", "Oct", "Nov", "Dec"]; 
	var curMonth = new Date().getMonth();
	var curYear = new Date().getFullYear();
	var curDay = new Date().getDate();
	$scope.currentDate = curDay + " " + monthNames[curMonth] + ", " +  curYear;
	var diff = new Date(curYear,curMonth,curDay) - new Date('1990-01-01 00:00:00');
	var numOfdays = ((((diff / 1000) / 60) / 60) / 24);
	$scope.getStudentHomework = function(){
		$http.get(APIUrl+'ViewStudentHomeWork/'+numOfdays)
		.success(function(data){
			$ionicLoading.hide();
			$scope.homework_available=true;				
			if(data.length==0){
				$ionicPopup.alert({
					template:"No homeWork for this day",					
				});		
			}else{
				$scope.homework_data=data;
				$scope.homework_available=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
		});
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
      		updateSelectedDate(val);
        	console.log('Return value from the datepicker popup is : ' + val, new Date(val));
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    function updateSelectedDate(val){
    	var selectedDate = new Date(val);
    	curMonth = monthNames[selectedDate.getMonth()];
		curYear = selectedDate.getFullYear();
		curDay = selectedDate.getDate();
		$scope.currentDate = curDay + " " + curMonth + ", " +  curYear;
		numOfdays = getNumberOfDays(selectedDate);
		$scope.getStudentHomework();
    }

    function getNumberOfDays(selectedDate){
    	var diff = selectedDate - new Date('1990-01-01 00:00:00');
		return ((((diff / 1000) / 60) / 60) / 24);	
    }
    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };

    $scope.go_backward = function(){
        numOfdays--;
        var newDate = new Date('1990-01-01 00:00:00');
        newDate.setDate(newDate.getDate() + numOfdays);
        updateSelectedDate(newDate);
        // $scope.getStudentHomework();
    }

    $scope.go_forward = function(){
        numOfdays++;
        var newDate = new Date('1990-01-01 00:00:00');
        newDate.setDate(newDate.getDate() + numOfdays);
        updateSelectedDate(newDate);
        // $scope.getStudentHomework();
    }
})
