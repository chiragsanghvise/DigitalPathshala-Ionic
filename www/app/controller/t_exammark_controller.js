angular.module('starter')

.controller('TExammarkCtrl', function($scope, $state, $ionicPopup, $ionicLoading, $location, TExammarkService,APIUrl) {

	$scope.standard = "";
	$scope.chapters = [];
	$scope.std_div = "";
	$scope.is_std_selected = false;
	$scope.is_date_selected = false;
	$scope.is_exam_selected = false;
	$scope.is_schedule_selected = false;
	$scope.students = {};
	// call service method to get menu_list and pass it to html page
	$ionicLoading.show();
	TExammarkService.get_standard_list().then(function(standards){
		$ionicLoading.hide();
		if (JSON.stringify(standards)!="[]") {
			$scope.standard_list = standards;
			$scope.hasStandard=true;	
		}else{
			$scope.hasStandard=false;
			$scope.is_std_selected=false;
			$scope.is_exam_selected = false;
			$scope.is_schedule_selected = false;
			$ionicPopup.alert({
				template:"Standard Data is not available",					
			});
		};
		
	},function(msg){
		$ionicLoading.hide();
		console.log("ctrl","msg");
	});

	$scope.split_id = function(data){
		if(!data.Id){
			$ionicPopup.alert({
				template:"Select standard",					
			});
			return;
		}
		ary = data['Id'].split('_');
		$scope.standard = ary[0];
		$scope.std_div = ary[1];
		$scope.is_std_selected = true;
		$ionicLoading.show();
		TExammarkService.get_exam_list($scope.standard).then(function(exam_list){
			$ionicLoading.hide();
			if (JSON.stringify(exam_list)!="[]") {
				$scope.is_std_selected=true;
				$scope.is_exam_selected=false;
				$scope.is_schedule_selected=false;
				$scope.exam_list = exam_list;
			}else{
				$scope.is_std_selected=false;
				$scope.is_exam_selected = false;
				$scope.is_schedule_selected = false;
				$ionicPopup.alert({
					template:"Exam Data is not available",					
				});
			};
			
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
		});
	}


	$scope.get_exam_schedule = function(data){
		if(!data || !data.Id)
			return;
		$scope.exam_id = data['Id'];
		var info = {
			'std':$scope.standard,
			'exam_id':data['Id']
		};
		$ionicLoading.show();
		TExammarkService.get_exam_schedule(info).then(function(schedule_list){
			$ionicLoading.hide();
			if (JSON.stringify(schedule_list)!="[]") {
				$scope.is_exam_selected = true;
				$scope.is_schedule_selected=false;
				$scope.schedule_list = schedule_list;	
			}else{
				$scope.is_exam_selected = false;
				$scope.is_schedule_selected = false;	
				$ionicPopup.alert({
					template:"Exam Schedule is not Declared",					
				});
			};
			
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
		});		
	}

	$scope.updateMarks = function(studentId){
		for (var i = 0; i < $scope.student_list.length; i++) {
			if($scope.student_list[i]['StudentId'] == studentId){
				$scope.marks_detail['Students'][i]['TheoryMarks'] = $scope.students[studentId];
			}
		};
	}

	$scope.get_student_list = function(data){
		// $scope.is_schedule_selected = true;
		var info = {
			'std':$scope.standard,
			'exam_id':$scope.exam_id,
			'cls':$scope.std_div,
			'scheduleid':data['Id']
		};
		$ionicLoading.show();
		TExammarkService.get_student_list(info).then(function(marks_detail){
			$ionicLoading.hide();
			if(JSON.stringify(marks_detail)!="null"){
				$scope.is_schedule_selected = true;
				$scope.marks_detail = marks_detail;
				$scope.student_list = marks_detail['Students'];		
			}else{
				$scope.is_schedule_selected = false;
				$ionicPopup.alert({
					template:"No stduent registered for this exam",					
				});			
			}
			

		},function(msg){
			$ionicLoading.hide();
		});

	}

	$scope.send_data = function(){
		$ionicLoading.show();
		TExammarkService.update_students_marks($scope.marks_detail).then(function(response){
			$ionicLoading.hide();
		},function(msg){
			console.log("ctrl","msg");
		});	
	}

})
 
