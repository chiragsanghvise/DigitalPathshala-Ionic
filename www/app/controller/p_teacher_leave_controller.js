angular.module('starter')
.controller('PTLeaveCtrl', function($scope, $state,$http ,$ionicPopup,$ionicLoading, PTLeaveService,APIUrl) {
	//var api_url="http://school.jmsofttech.com/api/";
	$scope.isLeaveDetail = false;
	$scope.has_prev=false;
	$scope.has_next=true;
	$scope.leave_details = [];
	$ionicLoading.show();
	$scope.current_counter = 0;
	$scope.leaveApproved=[];
	$http.get(APIUrl+'Teacher')
		.success(function(data){
			$ionicLoading.hide();
			if(data!="[]"){
				$scope.isTeacherList=true;
				$scope.teacher_list=data;
			}else{
				$scope.isTeacherList=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
		})

	$scope.getTeacherId = function(teacher){
		PTLeaveService.get_leave_details(teacher).then(function(leave_details){
			if (JSON.stringify(leave_details)!="[]") {
				$scope.isTeacherList = false;
				$scope.isLeaveDetail = true;
				$scope.leaveApproved[$scope.current_counter]=leave_details[$scope.current_counter]['IsAccepted'];
				$scope.leave_details = leave_details;	
			}else{
				$ionicPopup.alert({
					template:"No Leave Apply by this teacher",					
				});
				$scope.isTeacherList = true;
				$scope.isLeaveDetail = false;	
			};
		
		});
	}
	$scope.leaveAccept = function(leave_data){
		leave_data.IsAccepted=1;
		PTLeaveService.post(leave_data).then(function(leave_status){
				$scope.leaveApproved[$scope.current_counter]=1;
				$scope.isApproved=true;
				$ionicPopup.alert({
					template:leave_status,
				});	
		});	
	}

	$scope.leaveReject = function(leave_data){
		leave_data.IsAccepted=2;
		PTLeaveService.post(leave_data).then(function(leave_status){
				$scope.leaveApproved[$scope.current_counter]=2;
				$scope.isApproved=false;
				$ionicPopup.alert({
					template:"Application Rejected",
				});	
		});	
	}
	$scope.go_backward = function(){
		if($scope.current_counter > 0){
			$scope.current_counter=$scope.current_counter-1;
			$scope.has_next = true;
		}
		if($scope.current_counter == 0){
			$scope.has_prev = false;
		}
		if ($scope.leaveApproved[$scope.current_counter]==1) {
			$scope.isApproved=true;
		}else{
			$scope.isApproved=false;
		};

	}

	$scope.go_forward = function(){
		
		if($scope.current_counter < $scope.leave_details.length-1){
			$scope.current_counter=$scope.current_counter+1;
			$scope.has_prev = true;
		}
		if($scope.current_counter == $scope.leave_details.length-1){
			$scope.has_next = false;
		}
		if ($scope.leaveApproved[$scope.current_counter]==1) {
			$scope.isApproved=true;
		}else{
			$scope.isApproved=false;
		};

	}	
})