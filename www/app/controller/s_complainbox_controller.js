angular.module('starter')

.controller('SComplainBox', function($scope, $state,$http ,$ionicPopup,$ionicLoading,ComplainBoxService) {
	$scope.sendComplain=function(complain){
		//$ionicLoading.show();
		if (complain.message=="") {
			$ionicPopup.alert({
				template:"Please enter complain",					
			});
		}else{
			ComplainBoxService.sendComplainMessage(complain).then(function(status){
				$ionicLoading.hide();
				$ionicPopup.alert({
					title:'Success',
					template:status,					
				});

			})
			complain.message="";
		}	
	}
})