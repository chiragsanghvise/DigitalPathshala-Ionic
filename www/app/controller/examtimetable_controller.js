angular.module('starter')

.controller('ExamTimeTableCtrl', function($scope, $state, $ionicLoading,$ionicPopup, $window, $location, examTimeTableService) {
	$scope.schedule_list = [];
	$ionicLoading.show();
	// call service method to get menu_list and pass it to html page
	examTimeTableService.get_exam_tt_data().then(function(dashboard_data){
		$ionicLoading.hide();
		if (dashboard_data=="[]") {
			$ionicPopup.alert({
				template:"Exam is Not Available",					
			});					
		}else{
			$scope.timtble_list = dashboard_data;			
		}
	},function(msg){
		$ionicLoading.hide();
	});

	// method to get data of selected option
	$scope.display_info = function(data){
		$ionicLoading.show();
		examTimeTableService.get_schedule(data).then(function(schedule_data){
	//		console.log(schedule_data);
			$ionicLoading.hide();
			if (schedule_data.length==0) {
				$ionicPopup.alert({
					template:"Exam Schedule is Not Available",					
				});					
			}else{
			$scope.schedule_list= schedule_data;			
			}	
		
		});
		// $scope.schedule_list=examTimeTableService.get_schedule(data)
	}
})
 
