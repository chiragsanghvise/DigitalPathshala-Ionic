appMain.controller('TUploadPaperCtrl', function ($scope, $state, $ionicLoading, $location, TUploadPaperService, APIUrl) {
    $scope.standard = "";
    // call service method to get menuList and pass it to html page
    $ionicLoading.show();
    TUploadPaperService.GetStandardList().then(function (standards) {
        $ionicLoading.hide();
        if (JSON.stringify(standards) != "[]") {
             console.log("DATA Available" + standards);               
            $scope.standard_list = standards;
        } else {
             console.log("DATA Not Available");   
            /*Toast.alert({
                template: "Standard Data is not available",
            });*/
        };

    }, function (msg) {
        $ionicLoading.hide();
        console.log("ctrl", "msg");
    });

})

