﻿(function (app) {
    app.factory('httpRequestInterceptor', ['$q', '$rootScope', '$timeout',
    function ($q, $rootScope, $timeout) {
        return {
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                return rejection || $q.when(rejection);
            },
            request: function (request) {                
                if (request.url != null && request.url.indexOf("/AuthorizationToken") > -1) {
                    request.headers['Login-Token'] = "1-Login";
                }else{
                    request.headers['Authorization-Token'] = window.localStorage.getItem('Token');
                    // request.headers['Authorization-Token'] = "80,120,198,186,105,249,234,137,141,71,171,6,80,200,200,215,91,16,193,38,6,200,154,232,112,255,244,28,26,28,235,249,156,48,252,89,208,146,236,147,199,250,201,19,208,54,48,127,242,227,4,168,186,52,52,95,147,139,87,17,184,187,228,105,134,100,47,145,7,116,155,94,250,157,115,86,127,87,165,223,251,18,108,90,92,121,91,113,218,90,89,79,132,73,96,246,246,123,199,110,151,139,160,205,118,169,49,241,66,39,121,216,1,228,253,23,49,21,50,136,248,76,30,98,60,5,216,82";;
                }
                return request || $q.when(request);
            },
            requestError: function (rejection) {
                $q.reject(rejection);
             }
        };
    }]);
})(appMain);